<?php

return array(
// General 
	"norecord" => "No se encontró ningún registro",
	"create" => "Crear nuevo",

	// General , Login Info & Signup
	"home" => "Home",
	"group" => "Grupo",
	"username" => "nombre de usuario",
	"email" => "Dirección de correo electrónico",
	"password" => "Contraseña",
	"repassword" => "Confirmar contraseña",
	"forgotpassword" => "Olvidó su contraseña",
	"newpassword" => "Nueva contraseña",
	"conewpassword" => "Confirmar contraseña",
	"notepassword" => "Déjelo en blanco si no desea cambiar la contraseña actual", // updated apidevlab	
	"submit" => "Enviar",
	"signin" => "Iniciar sesión",
	"signup" => "Registrarse",
	"subscribe" => "Suscribir",
	"language" => "Idioma",
	"subscription" => "Suscripción",
	"firstname" => "Nombre",
	"lastname" => "Apellido",
	"lastlogin"	=> "Último inicio de sesión",
	"personalinfo"	=> "Información personal",
	"changepassword"	=> "Cambiar contraseña",
	"registernew" => "Registrar nueva cuenta ",
	"backtosite" => " Volver al sitio",
	"planexpiration" => "Plan de caducidad",
	'invitationmail' => 'Correo de invitación',
	
/* grid , pagination */
	"grid_displaying" 	=> "Mostrando",
	"grid_to" 			=> "A",
	"grid_of" 			=> "De",
	"grid_show" 			=> "Mostrar",
	"grid_sort" 			=> "Ordenar",
	"grid_order" 			=> "Pedido",	
	"grid_page" 			=> "Página",	
		

/* Menu navigation here */
	"m_controlpanel" => "Panel de control",
	"m_dashboard" => "Tablero",
	"m_setting" => "Configuración", // actualizado apidevlab
	"m_usersgroups" => "Usuarios y grupos",
	"m_users" => "Usuarios",
	"m_groups" => "Grupos",
	"m_pagecms" => "Página CMS",
	"m_menu" => "Administración de menús",
	"m_logs" => "Registros de actividad",
	"m_codebuilder" => "Generador de código",
	"m_blastemail" => "Blast Email",
	"m_myaccount" => "Mi cuenta",
	"m_logout" => "Cerrar sesión",
	"m_profile" => "Perfil",
	"m_manual" => "Guía manual",


/* Setting page translation */	
"t_generalsetting" => "Configuración general", // actualizado apidevlab
"t_generalsettingsmall" => "Administrar configuración de configuraciones", // actualizado apidevlab
"t_blastemail" => "Blast Email", // actualización de apidevlab
"t_blastemailsmall" => "Enviar correo masivo",
"t_emailtemplate" => "Plantillas de correo electrónico",
"t_emailtemplatesmall" => "Administrar plantillas de correo electrónico", // actualización de apidevlab
"t_loginsecurity" => "Inicio de sesión y seguridad", // actualizado apidevlab
"t_loginsecuritysmall" => "Administrar inicios de sesión y seguridad", // actualizado apidevlab
"t_socialmedia" => "Inicio de sesión en redes sociales", // actualización de apidevlab
"t_lfb" => "Iniciar sesión en Facebook", // actualización de apidevlab
"t_lgoogle" => "Iniciar sesión a través de Google", // actualización de apidevlab
"t_ltwit" => "Iniciar sesión a través de Twitter", // actualización de apidevlab
"tab_siteinfo" => "Configuración general", // Información del sitio actualizada apidevlab
"tab_loginsecurity" => "Inicio de sesión y seguridad",
"tab_email" => "Plantillas de correo electrónico", // actualización de apidevlab
"tab_translation" => "Traducción",
"fr_appname" => "Nombre de la aplicación",
"fr_appdesc" => "Desc aplicación",
"fr_comname" => "Nombre de la compañía",
"fr_emailsys" => "Sistema de correo electrónico",
"fr_emailmessage" => "Mensaje de correo electrónico",
"fr_enable" => "Habilitar",
"fr_multilanguage" => "Lenguaje Muliti",
"fr_mainlanguage" => "Idioma principal",
"fr_fronttemplate" => "Plantilla frontend",
"fr_appmode" => "Modo de aplicación",
"fr_appid" => "ID de la aplicación",
"fr_secret" => "NÚMERO SECRETO",
"fr_registrationdefault" => "Registro de grupo predeterminado",
"fr_registrationsetting" => "Configuración de registro",
"fr_registration" => "Registro",
"fr_allowregistration" => "Permitir registro",
"fr_allowfrontend" => "Permitir frontend",
"fr_registrationauto" => "Activación automática",
"fr_registrationmanual" => "Activación manual",
"fr_registrationemail" => "Correo electrónico con enlace de activación",
"fr_emailsubject" => "Asunto",
"fr_emailsendto" => "Enviar a",
"fr_emailmessage" => "Mensaje de correo electrónico",
"fr_emailtag" => "Puede usar",

	
/* submit */
"sb_savechanges" => "Guardar cambios",
"sb_send" => "Enviar",
"sb_save" => "Guardar",
"sb_apply" => "Aplicar cambios (s)",
"sb_submit" => "Enviar",
"sb_cancel" => "Cancelar",


/* button */
"btn_back" => "Atrás",
"btn_action" => "Acción",
"btn_search" => "Buscar",
"btn_clearsearch" => "Borrar búsqueda",
"btn_download" => "Descargar",
"btn_config" => "Configuración",
"btn_copy" => "Copiar", // Ajax
"btn_print" => "Imprimir",
"btn_create" => "Crear",
"btn_install" => "Instalar",
"btn_backup" => "Copia de seguridad",
"btn_remove" => "Eliminar",
"btn_edit" => "Editar",
"btn_view" => "Ver",
"btn_typesearch" => "Escriba y escriba", // actualizado apidevlab
	
/* Core Module */
"t_menu" => "Administración de menús",
"t_menusmall" => "Lista de todos los menús",
"t_tipsdrag" => "Arrastrar y soltar para reordenar la lista de menú", // actualizado apidevlab
"t_tipsnote" => "¡Atención! Los menús solo admiten 3 niveles", // actualización de apidevlab
"tab_topmenu" => "Menú principal",
"tab_sidemenu" => "Menú lateral",
"sb_reorder" => "Reordenar menú",
"fr_mtitle" => "Nombre / Título",
"fr_mtype" => "Tipo de menú",
"fr_mposition" => "Posición",
"fr_mactive" => "Activo",
"fr_minactive" => "Inactivo",
"fr_maccess" => "Acceso", // actualizado apidevlab
"fr_miconclass" => "Clase de icono",
"fr_mpublic" => "Público",
"fr_mexample" => "Ejemplo",
"fr_musage" => "Uso", // actualizado apidevlab
	
/* Code BuilderModule */
"t_module" => "Módulo",
"t_modulesmall" => "Lista de todos los módulos", // actualizado apidevlab
"tab_installed" => "Módulos instalados", // actualizado apidevlab
"tab_core" => "Módulos principales", // actualización de apidevlab
"fr_modtitle" => "Nombre / título del módulo",
"fr_modnote" => "Nota del módulo",
"fr_modtable" => "Tabla de módulos",
"fr_modautosql" => "Auto Mysql Statment",
"fr_modmanualsql" => "Manual Mysql Statment",
"fr_createmodule" => "Crear nuevo módulo",
"fr_installmodule" => "Instalar módulo",
"fr_backupmodule" => "Copia de seguridad y crear instalador para módulos",
/* dashboard Interface */

"dash_i_module" => "Módulo",
"dash_i_setting" => "Configuración",
"dash_i_sitemenu" => "Menú del sitio",
"dash_i_usergroup" => "Usuario y grupo",
"dash_module" => "Administrar módulos existentes o crear uno nuevo",
"dash_setting" => "Configuración de la opción de inicio de sesión de la aplicación, nombre del sitio, correo electrónico, etc.",
"dash_sitemenu" => "Administrar menú para su aplicación frontend o backend",
"dash_usergroup" => "Administrar grupos y usuarios y otorgar qué módulo y menú son accesibles",

/*updates	on may ,5 2014 */
	
"loginsocial" => "Iniciar sesión a través de las redes sociales", // actualización de apidevlab
"enteremailforgot" => "Ingrese su dirección de correo electrónico",
"detalle" => "Ver detalle",
"addedit" => "Agregar - Editar",

/* Notification */
"note_noexists" => "Lo sentimos, ¡la página no existe!", // actualización de apidevlab
"note_restric" => "Lo sentimos, no tiene permiso para acceder a esta página", // actualizado apidevlab
"note_success" => "Guardado con éxito!", // actualizado apidevlab
"note_error" => "¡Han ocurrido los siguientes errores!",
"note_success_delete" => "Eliminado correctamente!", // actualizado apidevlab
"connect_stripe_note" => "Primero debe conectarse a stripe.",
"accept_payment" => "El miembro no puede aceptar el pago en este momento.",
);