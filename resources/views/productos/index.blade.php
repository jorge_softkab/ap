
@extends('layouts.app')

@section('content')
{{--*/ usort($tableGrid, "SiteHelpers::_sort") /*--}}
  <div class="page-content row">
  	<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
  	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">

  	<script>
  		$(document).ready(function() {
			    $('#example').DataTable();
		} );
  	</script>
	<table id="example" class="display" style="width:100%">
        <thead>
            <tr>
            	<th></th>
                <th>Nombre</th>
                <th>Resumen</th>
                <th>Editar</th>
                <th>Ver Más</th>
               
            </tr>
        </thead>
        <?php $productos = App\Models\Productos::all(); 
		     


		  ?>
        <tbody>
        	@foreach($productos as $producto)
            <tr>
            <td><img src="{{ $producto->url_image }}" alt="{{ $producto->nombre }}" width="200"></td>
			<td>{{ $producto->nombre }}</td>
			<td>{{ $producto->resumen }}</td>
            <td><a href="/productos/update/{{ $producto->id }}">Editar</a></td>
            <td><a href="/productos/show/{{ $producto->id }}">Ver más</a></td>
            </tr>
            @endforeach
           
        </tbody>
        <tfoot>
            <tr>
               <th></th>
                <th>Nombre</th>
                <th>Resumen</th>
                <th>Editar</th>
                <th>Ver Más</th>
                
                
            </tr>
        </tfoot>
    </table>
	

</div>	
	
@stop