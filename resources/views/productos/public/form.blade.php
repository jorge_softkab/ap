

		 {!! Form::open(array('url'=>'productos/savepublic', 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}

	@if(Session::has('messagetext'))
	  
		   {!! Session::get('messagetext') !!}
	   
	@endif
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		


<div class="col-md-12">
						<fieldset><legend> productos</legend>
									
									  <div class="form-group  " >
										<label for="Id" class=" control-label col-md-4 text-left"> Id </label>
										<div class="col-md-7">
										  <input  type='text' name='id' id='id' value='{{ $row['id'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Nombre" class=" control-label col-md-4 text-left"> Nombre </label>
										<div class="col-md-7">
										  <input  type='text' name='nombre' id='nombre' value='{{ $row['nombre'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Idcategoria" class=" control-label col-md-4 text-left"> Idcategoria </label>
										<div class="col-md-7">
										  <select name='idcategoria[]' multiple rows='5' id='idcategoria' class='select2 '   ></select> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Idsubcategoria" class=" control-label col-md-4 text-left"> Idsubcategoria </label>
										<div class="col-md-7">
										  <input  type='text' name='idsubcategoria' id='idsubcategoria' value='{{ $row['idsubcategoria'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Idsubsubcategoria" class=" control-label col-md-4 text-left"> Idsubsubcategoria </label>
										<div class="col-md-7">
										  <input  type='text' name='idsubsubcategoria' id='idsubsubcategoria' value='{{ $row['idsubsubcategoria'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Url" class=" control-label col-md-4 text-left"> Url </label>
										<div class="col-md-7">
										  <input  type='text' name='url' id='url' value='{{ $row['url'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Url Image" class=" control-label col-md-4 text-left"> Url Image </label>
										<div class="col-md-7">
										  <input  type='file' name='url_image' id='url_image' @if($row['url_image'] =='') class='required' @endif style='width:150px !important;'  />
					 	<div >
						{!! SiteHelpers::showUploadedFile($row['url_image'],'') !!}
						
						</div>					
					 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Resumen" class=" control-label col-md-4 text-left"> Resumen </label>
										<div class="col-md-7">
										  <textarea name='resumen' rows='5' id='editor' class='form-control editor '  
						 >{{ $row['resumen'] }}</textarea> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Descripcion" class=" control-label col-md-4 text-left"> Descripcion </label>
										<div class="col-md-7">
										  <textarea name='descripcion' rows='5' id='editor' class='form-control editor '  
						 >{{ $row['descripcion'] }}</textarea> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Ventajas" class=" control-label col-md-4 text-left"> Ventajas </label>
										<div class="col-md-7">
										  <textarea name='ventajas' rows='5' id='editor' class='form-control editor '  
						 >{{ $row['ventajas'] }}</textarea> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Especificaciones" class=" control-label col-md-4 text-left"> Especificaciones </label>
										<div class="col-md-7">
										  <textarea name='especificaciones' rows='5' id='editor' class='form-control editor '  
						 >{{ $row['especificaciones'] }}</textarea> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Created At" class=" control-label col-md-4 text-left"> Created At </label>
										<div class="col-md-7">
										  
				<div class="input-group m-b" style="width:150px !important;">
					{!! Form::text('created_at', $row['created_at'],array('class'=>'form-control datetime', 'style'=>'width:150px !important;')) !!}
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				</div>
				 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Updated At" class=" control-label col-md-4 text-left"> Updated At </label>
										<div class="col-md-7">
										  
				<div class="input-group m-b" style="width:150px !important;">
					{!! Form::text('updated_at', $row['updated_at'],array('class'=>'form-control datetime', 'style'=>'width:150px !important;')) !!}
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				</div>
				 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> </fieldset>
			</div>
			
			

			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
				  </div>	  
			
		</div> 
		 
		 {!! Form::close() !!}
		 
   <script type="text/javascript">
	$(document).ready(function() { 
		
		
		$("#idcategoria").jCombo("{!! url('productos/comboselect?filter=categorias:id:nombre') !!}",
		{  selected_value : '{{ $row["idcategoria"] }}' });
		 

		$('.removeCurrentFiles').on('click',function(){
			var removeUrl = $(this).attr('href');
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	</script>		 
