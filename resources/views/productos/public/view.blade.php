<div class="m-t" style="padding-top:25px;">	
    <div class="row m-b-lg animated fadeInDown delayp1 text-center">
        <h3> {{ $pageTitle }} <small> {{ $pageNote }} </small></h3>
        <hr />       
    </div>
</div>
<div class="m-t">
	<div class="table-responsive" > 	

		<table class="table table-striped table-bordered" >
			<tbody>	
		
			
					<tr>
						<td width='30%' class='label-view text-right'>Nombre</td>
						<td>{{ $row->nombre}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Idcategoria</td>
						<td>{{ $row->idcategoria}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Idsubcategoria</td>
						<td>{{ $row->idsubcategoria}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Idsubsubcategoria</td>
						<td>{{ $row->idsubsubcategoria}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Url</td>
						<td>{{ $row->url}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Url Image</td>
						<td><a href="">{{ $row->url_image}} </a> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Resumen</td>
						<td>{{ $row->resumen}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Descripcion</td>
						<td>{{ $row->descripcion}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Ventajas</td>
						<td>{{ $row->ventajas}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Especificaciones</td>
						<td>{{ $row->especificaciones}} </td>
						
					</tr>
						
					<tr>
						<td width='30%' class='label-view text-right'></td>
						<td> <a href="javascript:history.go(-1)" class="btn btn-primary"> Back To Grid <a> </td>
						
					</tr>					
				
			</tbody>	
		</table>   

	 
	
	</div>
</div>	