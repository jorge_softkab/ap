@extends('layouts.app')

@section('content')
<div class="page-content row">
    <!-- Page header -->

	 
	 
 	<div class="page-content-wrapper m-t">   

<div class="sbox ">
	<div class="sbox-title">
		<div class="sbox-tools pull-left" >
	   		<a href="{{ url('productos?return='.$return) }}" class="tips btn btn-xs btn-default" title="{{ Lang::get('core.btn_back') }}"><i class="fa  fa-arrow-left"></i></a>
			@if($access['is_add'] ==1)
	   		<a href="{{ url('productos/update/'.$id.'?return='.$return) }}" class="tips btn btn-xs btn-default" title="{{ Lang::get('core.btn_edit') }}"><i class="fa  fa-pencil"></i></a>
			@endif 
					
		</div>	

		<div class="sbox-tools " >
			<a href="{{ ($prevnext['prev'] != '' ? url('productos/show/'.$prevnext['prev'].'?return='.$return ) : '#') }}" class="tips btn btn-xs btn-default"><i class="fa fa-arrow-left"></i>  </a>	
			<a href="{{ ($prevnext['next'] != '' ? url('productos/show/'.$prevnext['next'].'?return='.$return ) : '#') }}" class="tips btn btn-xs btn-default"> <i class="fa fa-arrow-right"></i>  </a>
			@if(Session::get('gid') ==1)
				<a href="{{ URL::to('sximo/module/config/'.$pageModule) }}" class="tips btn btn-xs btn-default" title=" {{ Lang::get('core.btn_config') }}" ><i class="fa  fa-ellipsis-v"></i></a>
			@endif 			
		</div>


	</div>
	<div class="sbox-content" > 	

		<table class="table table-striped table-bordered" >
			<tbody>	
		
					<tr>
						<td width='30%' class='label-view text-right'>Nombre</td>
						<td>{{ $row->nombre}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Idcategoria</td>
						<td>{{ $row->idcategoria}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Idsubcategoria</td>
						<td>{{ $row->idsubcategoria}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Idsubsubcategoria</td>
						<td>{{ $row->idsubsubcategoria}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Url</td>
						<td>{{ $row->url}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Url Image</td>
						<td><a href="">{{ $row->url_image}} </a> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Resumen</td>
						<td>{{ $row->resumen}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Descripcion</td>
						<td>{{ $row->descripcion}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Ventajas</td>
						<td>{{ $row->ventajas}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Especificaciones</td>
						<td>{{ $row->especificaciones}} </td>
						
					</tr>
				
			</tbody>	
		</table>   

	 
	
	</div>
</div>	

	</div>
</div>
	  
@stop