
@extends('layouts.app')

@section('content')
{{--*/ usort($tableGrid, "SiteHelpers::_sort") /*--}}
  <div class="page-content row">
  	<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
  	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">

  	<script>
  		$(document).ready(function() {
			    $('#example').DataTable();
		} );
  	</script>
	<table id="example" class="display" style="width:100%">
        <thead>
            <tr>
                <th>Nombre</th>
                <th>Resumen</th>
                <th>Imagen</th>
                <th>Ventajas</th>
                <th>Especificaciones</th>
                <th>Salary</th>
            </tr>
        </thead>
        <?php $productos = App\Models\Productos::all(); 
		     


		  ?>
        <tbody>
        	@foreach($productos as $producto)
            <tr>
			<td>{{ $producto->nombre }}</td>
			<td>{{ $producto->resumen }}</td>
            <td>{{ $producto->imagen }}</td>
            <td>{{ $producto->ventajas }}</td>
            <td>{{ $producto->especificaciones }}</td>
            </tr>
            @endforeach
           
        </tbody>
        <tfoot>
            <tr>
               <th>Nombre</th>
                <th>Resumen</th>
                <th>Imagen</th>
                <th>Ventajas</th>
                <th>Especificaciones</th>
                <th>Salary</th>
                
            </tr>
        </tfoot>
    </table>
	

</div>	
	
@stop