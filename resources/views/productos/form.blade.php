@extends('layouts.app')

@section('content')

  <div class="page-content row">
    <!-- Page header -->

 
 	<div class="page-content-wrapper m-t">


<div class="sbox">
	<div class="sbox-title"> 
		<div class="sbox-tools pull-left" >
			<a href="{{ url($pageModule.'?return='.$return) }}" class="tips btn btn-xs btn-default"  title="{{ Lang::get('core.btn_back') }}" ><i class="fa  fa-arrow-left"></i></a> 
		</div>
		<div class="sbox-tools " >
			@if(Session::get('gid') ==1)
				<a href="{{ URL::to('sximo/module/config/'.$pageModule) }}" class="tips btn btn-xs btn-default" title=" {{ Lang::get('core.btn_config') }}" ><i class="fa  fa-ellipsis-v"></i></a>
			@endif 			
		</div> 

	</div>
	<div class="sbox-content"> 	

		<ul class="parsley-error-list">
			@foreach($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>	

		 {!! Form::open(array('url'=>'productos/save?return='.$return, 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
<div class="col-md-12">
						<fieldset><legend> productos</legend>
									
									  <div class="form-group  " >
										<label for="Id" class=" control-label col-md-4 text-left"> Id </label>
										<div class="col-md-7">
										  <input  type='text' name='id' id='id' value='{{ $row['id'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Nombre" class=" control-label col-md-4 text-left"> Nombre </label>
										<div class="col-md-7">
										  <input  type='text' name='nombre' id='nombre' value='{{ $row['nombre'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Idcategoria" class=" control-label col-md-4 text-left"> Categorias </label>
										<div class="col-md-7">
										  <select name='idcategoria[]' multiple rows='5' id='idcategoria' class='select2 '   ></select> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Idsubcategoria" class=" control-label col-md-4 text-left"> Subcategorias </label>
										<div class="col-md-7">
										  <select name='idsubcategoria[]' multiple rows='5' id='idsubcategoria' class='select2 '   ></select> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Idsubsubcategoria" class=" control-label col-md-4 text-left"> Subsubcategorias </label>
										<div class="col-md-7">
										  <select name='idsubsubcategoria[]' multiple rows='5' id='idsubsubcategoria' class='select2 '   ></select> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Url" class=" control-label col-md-4 text-left"> Url </label>
										<div class="col-md-7">
										  <input  type='text' name='url' id='url' value='{{ $row['url'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Url Image" class=" control-label col-md-4 text-left"> Url Image </label>
										<div class="col-md-7">
										  <input  type='file' name='url_image' id='url_image' @if($row['url_image'] =='') class='required' @endif style='width:150px !important;'  />
					 	<div >
						{!! SiteHelpers::showUploadedFile($row['url_image'],'/uploads/') !!}
						
						</div>					
					 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Resumen" class=" control-label col-md-4 text-left"> Resumen </label>
										<div class="col-md-7">
										  <textarea name='resumen' rows='5' id='editor' class='form-control editor '  
						 >{{ $row['resumen'] }}</textarea> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Descripcion" class=" control-label col-md-4 text-left"> Descripcion </label>
										<div class="col-md-7">
										  <textarea name='descripcion' rows='5' id='editor' class='form-control editor '  
						 >{{ $row['descripcion'] }}</textarea> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Ventajas" class=" control-label col-md-4 text-left"> Ventajas </label>
										<div class="col-md-7">
										  <textarea name='ventajas' rows='5' id='editor' class='form-control editor '  
						 >{{ $row['ventajas'] }}</textarea> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Especificaciones" class=" control-label col-md-4 text-left"> Especificaciones </label>
										<div class="col-md-7">
										  <textarea name='especificaciones' rows='5' id='editor' class='form-control editor '  
						 >{{ $row['especificaciones'] }}</textarea> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Created At" class=" control-label col-md-4 text-left"> Created At </label>
										<div class="col-md-7">
										  
				<div class="input-group m-b" style="width:150px !important;">
					{!! Form::text('created_at', $row['created_at'],array('class'=>'form-control datetime', 'style'=>'width:150px !important;')) !!}
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				</div>
				 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Updated At" class=" control-label col-md-4 text-left"> Updated At </label>
										<div class="col-md-7">
										  
				<div class="input-group m-b" style="width:150px !important;">
					{!! Form::text('updated_at', $row['updated_at'],array('class'=>'form-control datetime', 'style'=>'width:150px !important;')) !!}
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				</div>
				 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> </fieldset>
			</div>
			
			

		
			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="icon-checkmark-circle2"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="icon-bubble-check"></i> {{ Lang::get('core.sb_save') }}</button>
					<button type="button" onclick="location.href='{{ URL::to('productos?return='.$return) }}' " class="btn btn-warning btn-sm "><i class="icon-cancel-circle2 "></i>  {{ Lang::get('core.sb_cancel') }} </button>
					</div>	  
			
				  </div> 
		 
		 {!! Form::close() !!}
	</div>
</div>		 
</div>	
</div>	
<?php  use App\Models\Productos; 
if ($row['id']=="") {
		$cat=array(0);
		$subcat=array(0);
		$subsubcat=array(0);
	}else{
	$producto = Productos::find($row['id']);

	$cat=array();
	foreach ($producto->categorias as $categorias) {
	    $cate = array_push($cat, $categorias->id);   
	}

	$subcat=array();
	foreach ($producto->subcategorias as $subcategorias) {
	    $subcate = array_push($subcat, $subcategorias->id);   
	}

	$subsubcat=array();
	foreach ($producto->subsubcategorias as $subsubcategorias) {
	    $subsubcate = array_push($subsubcat, $subsubcategorias->id);   
	}

}


?>
   <script type="text/javascript">
	$(document).ready(function() { 
		
		
		$("#idcategoria").jCombo("{!! url('productos/comboselect?filter=categorias:id:nombre') !!}",
		{  selected_value : '<?php echo implode(",",$cat) ?>'}


			);
		
		$("#idsubcategoria").jCombo("{!! url('productos/comboselect?filter=subcategorias:id:nombre') !!}",
		{  selected_value : '<?php echo implode(",", $subcat) ?>'}
		);
		
		$("#idsubsubcategoria").jCombo("{!! url('productos/comboselect?filter=subsubcategorias:id:nombre') !!}",
		{  selected_value : '<?php echo implode(",", $subsubcat) ?>'}
		);
		 

		$('.removeMultiFiles').on('click',function(){
			var removeUrl = '{{ url("productos/removefiles?file=")}}'+$(this).attr('url');
			$(this).parent().remove();
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	</script>	


@stop