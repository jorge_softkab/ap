<?php 
define('CNF_APPNAME','Airon Tools');
define('CNF_APPDESC','Administrador de Herramientas ');
define('CNF_COMNAME','Airon Tools');
define('CNF_EMAIL','info@airontools.com');
define('CNF_METAKEY','airontools.com , Airon Tools  , Herramientas Airon Tools');
define('CNF_METADESC','Herramientas Industriales Airon Tools');
define('CNF_GROUP','3');
define('CNF_ACTIVATION','auto');
define('CNF_MULTILANG','0');
define('CNF_LANG','es');
define('CNF_REGIST','true');
define('CNF_FRONT','true');
define('CNF_RECAPTCHA','false');
define('CNF_THEME','default');
define('CNF_RECAPTCHAPUBLICKEY','');
define('CNF_RECAPTCHAPRIVATEKEY','');
define('CNF_MODE','production');
define('CNF_LOGO','backend-logo.png');
define('CNF_ALLOWIP','');
define('CNF_RESTRICIP','192.116.134 , 194.111.606.21 ');
define('CNF_MAIL','phpmail');
define('CNF_DATE','m/d/y');
?>