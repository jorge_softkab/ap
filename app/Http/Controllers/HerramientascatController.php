<?php namespace App\Http\Controllers;

use App\Http\Controllers\controller;
use App\Models\Categorias;
use App\Models\Subcategorias;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Validator, Input, Redirect ; 


class HerramientascatController extends Controller {



public function index($url){
    	header('Access-Control-Allow-Origin: *'); 
    	$categoria = Categorias::where('url', $url) ->first();
        //return view('home', compact('productos', 'categorias'));
        return $categoria->productos;
 }


public function subcategorias($url){
    	header('Access-Control-Allow-Origin: *'); 
    	$subcategoria = Subcategorias::where('url', $url) ->first();
        //return view('home', compact('productos', 'categorias'));
        return $subcategoria->productos;
 }
		


}