<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class productos extends Sximo  {
	
	protected $table = 'productos';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT productos.* FROM productos  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE productos.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	
	public function categorias()
    {
    return $this->belongsToMany('App\Models\Categorias');
    }
     public function subcategorias()
    {
    return $this->belongsToMany('App\Models\Subcategorias');
    }
     public function subsubcategorias()
    {
    return $this->belongsToMany('App\Models\Subsubcategorias');
    }
}
