<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class subsubcategorias extends Sximo  {
	
	protected $table = 'subsubcategorias';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT subsubcategorias.* FROM subsubcategorias  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE subsubcategorias.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	
	public function categorias()
    {
    return $this->belongsToMany('App\Models\Categorias');
    }

    public function subcategorias()
    {
    return $this->belongsToMany('App\Models\Subcategorias');
    }
}
