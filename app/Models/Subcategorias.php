<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class subcategorias extends Sximo  {
	
	protected $table = 'subcategorias';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT subcategorias.* FROM subcategorias  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE subcategorias.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
